1. Soal 1 Membuat Database
- CREATE DATABASE myshop;


2. Soal 2 Membuat(CREATE) Table di Dalam Database 
a. table users
- CREATE TABLE users ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );


b. table categories
- CREATE TABLE categories ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );


c. table items
- CREATE TABLE items ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price integer NOT null, stock integer NOT null, category_id int NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );


3. Soal 3 Memasukkan(INSERT) Data pada Table 
a. table users
- INSERT INTO users (name, email, password) VALUES("John Doe","john@doe.com","john123");
- INSERT INTO users (name, email, password) VALUES("Jane Doe","jane@doe.com","jenita123");

b. table categories
- INSERT INTO categories (name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

c. table items
- INSERT INTO items (name, description, price, stock, category_id) VALUES("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
- INSERT INTO items (name, description, price, stock, category_id) VALUES("Uniklooh","baju keren dari brand ternama",500000,50,2);
- INSERT INTO items (name, description, price, stock, category_id) VALUES("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Soal 4 Mengambil Data dari Database
a. Mengambil data users
- SELECT id, name, email from users;


b. Mengambil data items
- SELECT * FROM `items` WHERE price > 1000000;
- SELECT * FROM `items` WHERE name LIKE "%uniklo%";


c. Menampilkan data items join dengan kategori
- SELECT items.*, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id=categories.id;


5. Soal 5 Mengubah Data dari Database
- UPDATE items set price = 2500000 WHERE id = 1;

